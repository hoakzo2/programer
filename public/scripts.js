const input = document.getElementById("RSzoveg");
const highlight = document.getElementById("vSzovegOverlay");
const highbox = document.getElementById("highbox");

const cmds = [
    "print"
]

function onLoad() {
    console.log("Loaded");

    Update()
}

function Update() {
    const input = document.getElementById("RSzoveg");
    const highlight = document.getElementById("vSzovegOverlay");

    if (highlight.innerHTML != input.innerHTML) {
        highlight.innerHTML = input.innerHTML;
        console.log("Text changed.");
        HandleCommands();
    }

    ManageCheckHighBox();

    requestAnimationFrame(Update);
}

function HandleCommands() {
    const input = document.getElementById("RSzoveg");
    const highlight = document.getElementById("vSzovegOverlay");
    
    // A szöveg felosztása egyetlen sortörés ("\n") mentén
    lstd = input.innerText.split("\n");
    console.log(lstd);
}

function ManageCheckHighBox() {
    const input = document.getElementById("RSzoveg");
    const highlight = document.getElementById("vSzovegOverlay");
    const highbox = document.getElementById("highbox");

    if (!highbox.checked) {
        highlight.style.display = "none";
        input.opacity = 0;
    } else {
        highlight.style.display = "block";
        input.opacity = 1;
    }
}